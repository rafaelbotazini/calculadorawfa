﻿using System;
using System.Windows.Forms;

namespace CalculadoraWFA
{
    public partial class CalculadoraSimples : Form
    {
        Inteiro resultado = new Inteiro(0);
        Inteiro operando1 = new Inteiro(0);
        Inteiro operando2 = new Inteiro(0);
        Operacoes operacao;
        bool isOperacao = false;
        bool esperandoEntrada = false;

        public CalculadoraSimples()
        {
            InitializeComponent();
        }

        private void NumeroClick(object sender, EventArgs e)
        {
            if (display.Text == "0" || esperandoEntrada)
            {
                display.Text = "";
                esperandoEntrada = false;
            }

            display.Text += ((Button)sender).Text;


            int result;
            int.TryParse(display.Text, out result);

            if (!isOperacao)
            {
                operando1 = new Inteiro(result);
            }
            else
            {
                operando2 = new Inteiro(result);
            }
        }

        private void Calcular(Operacoes op)
        {
            if (!esperandoEntrada)
            {
                switch (op)
                {
                    case Operacoes.Soma:
                        SetResultado(operando1 + operando2);
                        break;
                    case Operacoes.Subtracao:
                        SetResultado(operando1 - operando2);
                        break;
                    case Operacoes.Multiplicacao:
                        SetResultado(operando1 * operando2);
                        break;
                    case Operacoes.Divisao:
                        {
                            if (operando2 > new Inteiro(0))
                            {
                                SetResultado(operando1 / operando2);
                            }
                            else throw new Exception("Impossível dividir por 0.");
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void SetResultado(Inteiro i)
        {
            resultado = i;
            operando1 = resultado;

            isOperacao = false;
            esperandoEntrada = true;

            display.Text = resultado.ToString();
        }

        private void SetOperacao(object sender, EventArgs e)
        {
            var btn = (Button)sender;

            switch (btn.Text)
            {
                case "+": { operacao = Operacoes.Soma; break; }
                case "-": { operacao = Operacoes.Subtracao; break; }
                case "*": { operacao = Operacoes.Multiplicacao; break; }
                case "/": { operacao = Operacoes.Divisao; break; }
            }

            isOperacao = true;
            esperandoEntrada = true;
        }

        private void buttonEquals_Click(object sender, EventArgs e)
        {
            try
            {
                Calcular(operacao);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                operando2 = new Inteiro(0);
                display.Text = "0";
            }
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            resultado = new Inteiro(0);
            operando1 = new Inteiro(0);
            operando2 = new Inteiro(0);
            isOperacao = false;
            esperandoEntrada = false;
            display.Text = "0";
        }

        private void buttonCE_Click(object sender, EventArgs e)
        {
            operando2 = new Inteiro(0);
            display.Text = "0";
            esperandoEntrada = true;
        }
    }
}
