﻿namespace CalculadoraWFA
{
    partial class ConversorBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.display = new System.Windows.Forms.Button();
            this.radioBin = new System.Windows.Forms.RadioButton();
            this.labelBin = new System.Windows.Forms.Label();
            this.radioOct = new System.Windows.Forms.RadioButton();
            this.labelOct = new System.Windows.Forms.Label();
            this.radioDec = new System.Windows.Forms.RadioButton();
            this.labelDec = new System.Windows.Forms.Label();
            this.radioHex = new System.Windows.Forms.RadioButton();
            this.labelHex = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button0 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.buttonC = new System.Windows.Forms.Button();
            this.buttonBackSpace = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // display
            // 
            this.display.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.display.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.display.Cursor = System.Windows.Forms.Cursors.Default;
            this.display.Enabled = false;
            this.display.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.display.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.display.Location = new System.Drawing.Point(17, 16);
            this.display.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.display.Name = "display";
            this.display.Size = new System.Drawing.Size(345, 55);
            this.display.TabIndex = 0;
            this.display.Text = "0";
            this.display.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.display.UseVisualStyleBackColor = false;
            this.display.TextChanged += new System.EventHandler(this.Calcular);
            // 
            // radioBin
            // 
            this.radioBin.AutoSize = true;
            this.radioBin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioBin.Location = new System.Drawing.Point(4, 4);
            this.radioBin.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioBin.Name = "radioBin";
            this.radioBin.Size = new System.Drawing.Size(54, 21);
            this.radioBin.TabIndex = 1;
            this.radioBin.Text = "BIN";
            this.radioBin.UseVisualStyleBackColor = true;
            this.radioBin.Click += new System.EventHandler(this.SelectBase);
            // 
            // labelBin
            // 
            this.labelBin.AutoSize = true;
            this.labelBin.Location = new System.Drawing.Point(107, 9);
            this.labelBin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelBin.Name = "labelBin";
            this.labelBin.Size = new System.Drawing.Size(16, 17);
            this.labelBin.TabIndex = 2;
            this.labelBin.Text = "0";
            // 
            // radioOct
            // 
            this.radioOct.AutoSize = true;
            this.radioOct.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioOct.Location = new System.Drawing.Point(4, 33);
            this.radioOct.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioOct.Name = "radioOct";
            this.radioOct.Size = new System.Drawing.Size(61, 21);
            this.radioOct.TabIndex = 1;
            this.radioOct.Text = "OCT";
            this.radioOct.UseVisualStyleBackColor = true;
            this.radioOct.Click += new System.EventHandler(this.SelectBase);
            // 
            // labelOct
            // 
            this.labelOct.AutoSize = true;
            this.labelOct.Location = new System.Drawing.Point(107, 38);
            this.labelOct.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelOct.Name = "labelOct";
            this.labelOct.Size = new System.Drawing.Size(16, 17);
            this.labelOct.TabIndex = 2;
            this.labelOct.Text = "0";
            // 
            // radioDec
            // 
            this.radioDec.AutoSize = true;
            this.radioDec.Checked = true;
            this.radioDec.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioDec.Location = new System.Drawing.Point(4, 62);
            this.radioDec.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioDec.Name = "radioDec";
            this.radioDec.Size = new System.Drawing.Size(60, 21);
            this.radioDec.TabIndex = 1;
            this.radioDec.TabStop = true;
            this.radioDec.Text = "DEC";
            this.radioDec.UseVisualStyleBackColor = true;
            this.radioDec.Click += new System.EventHandler(this.SelectBase);
            // 
            // labelDec
            // 
            this.labelDec.AutoSize = true;
            this.labelDec.Location = new System.Drawing.Point(107, 66);
            this.labelDec.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDec.Name = "labelDec";
            this.labelDec.Size = new System.Drawing.Size(16, 17);
            this.labelDec.TabIndex = 2;
            this.labelDec.Text = "0";
            // 
            // radioHex
            // 
            this.radioHex.AutoSize = true;
            this.radioHex.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioHex.Location = new System.Drawing.Point(4, 92);
            this.radioHex.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioHex.Name = "radioHex";
            this.radioHex.Size = new System.Drawing.Size(60, 21);
            this.radioHex.TabIndex = 1;
            this.radioHex.Text = "HEX";
            this.radioHex.UseVisualStyleBackColor = true;
            this.radioHex.Click += new System.EventHandler(this.SelectBase);
            // 
            // labelHex
            // 
            this.labelHex.AutoSize = true;
            this.labelHex.Location = new System.Drawing.Point(107, 97);
            this.labelHex.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelHex.Name = "labelHex";
            this.labelHex.Size = new System.Drawing.Size(16, 17);
            this.labelHex.TabIndex = 2;
            this.labelHex.Text = "0";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.radioHex);
            this.panel1.Controls.Add(this.radioBin);
            this.panel1.Controls.Add(this.labelHex);
            this.panel1.Controls.Add(this.radioOct);
            this.panel1.Controls.Add(this.labelDec);
            this.panel1.Controls.Add(this.labelOct);
            this.panel1.Controls.Add(this.radioDec);
            this.panel1.Controls.Add(this.labelBin);
            this.panel1.Location = new System.Drawing.Point(17, 79);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(345, 127);
            this.panel1.TabIndex = 3;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.Controls.Add(this.button0, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.button1, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.button2, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.button3, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.button4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.button5, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.button6, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.button7, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.button8, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.button9, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.button10, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.button11, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.button12, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.button13, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.button14, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.button15, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonC, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.buttonBackSpace, 2, 5);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(17, 213);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(345, 229);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // button0
            // 
            this.button0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button0.Location = new System.Drawing.Point(4, 194);
            this.button0.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(106, 31);
            this.button0.TabIndex = 0;
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = true;
            this.button0.Click += new System.EventHandler(this.EnterText);
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(4, 156);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 30);
            this.button1.TabIndex = 0;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.EnterText);
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Location = new System.Drawing.Point(118, 156);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(107, 30);
            this.button2.TabIndex = 0;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.EnterText);
            // 
            // button3
            // 
            this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button3.Location = new System.Drawing.Point(233, 156);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(108, 30);
            this.button3.TabIndex = 0;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.EnterText);
            // 
            // button4
            // 
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.Location = new System.Drawing.Point(4, 118);
            this.button4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(106, 30);
            this.button4.TabIndex = 0;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.EnterText);
            // 
            // button5
            // 
            this.button5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button5.Location = new System.Drawing.Point(118, 118);
            this.button5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(107, 30);
            this.button5.TabIndex = 0;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.EnterText);
            // 
            // button6
            // 
            this.button6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button6.Location = new System.Drawing.Point(233, 118);
            this.button6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(108, 30);
            this.button6.TabIndex = 0;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.EnterText);
            // 
            // button7
            // 
            this.button7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button7.Location = new System.Drawing.Point(4, 80);
            this.button7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(106, 30);
            this.button7.TabIndex = 0;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.EnterText);
            // 
            // button8
            // 
            this.button8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button8.Location = new System.Drawing.Point(118, 80);
            this.button8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(107, 30);
            this.button8.TabIndex = 0;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.EnterText);
            // 
            // button9
            // 
            this.button9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button9.Location = new System.Drawing.Point(233, 80);
            this.button9.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(108, 30);
            this.button9.TabIndex = 0;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.EnterText);
            // 
            // button10
            // 
            this.button10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button10.Location = new System.Drawing.Point(4, 42);
            this.button10.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(106, 30);
            this.button10.TabIndex = 0;
            this.button10.Text = "A";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.EnterText);
            // 
            // button11
            // 
            this.button11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button11.Location = new System.Drawing.Point(118, 42);
            this.button11.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(107, 30);
            this.button11.TabIndex = 0;
            this.button11.Text = "B";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.EnterText);
            // 
            // button12
            // 
            this.button12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button12.Location = new System.Drawing.Point(233, 42);
            this.button12.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(108, 30);
            this.button12.TabIndex = 0;
            this.button12.Text = "C";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.EnterText);
            // 
            // button13
            // 
            this.button13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button13.Location = new System.Drawing.Point(4, 4);
            this.button13.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(106, 30);
            this.button13.TabIndex = 0;
            this.button13.Text = "D";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.EnterText);
            // 
            // button14
            // 
            this.button14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button14.Location = new System.Drawing.Point(118, 4);
            this.button14.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(107, 30);
            this.button14.TabIndex = 0;
            this.button14.Text = "E";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.EnterText);
            // 
            // button15
            // 
            this.button15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button15.Location = new System.Drawing.Point(233, 4);
            this.button15.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(108, 30);
            this.button15.TabIndex = 0;
            this.button15.Text = "F";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.EnterText);
            // 
            // buttonC
            // 
            this.buttonC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonC.Location = new System.Drawing.Point(118, 194);
            this.buttonC.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonC.Name = "buttonC";
            this.buttonC.Size = new System.Drawing.Size(107, 31);
            this.buttonC.TabIndex = 0;
            this.buttonC.Text = "C";
            this.buttonC.UseVisualStyleBackColor = true;
            this.buttonC.Click += new System.EventHandler(this.buttonC_Click);
            // 
            // buttonBackSpace
            // 
            this.buttonBackSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonBackSpace.Location = new System.Drawing.Point(233, 194);
            this.buttonBackSpace.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonBackSpace.Name = "buttonBackSpace";
            this.buttonBackSpace.Size = new System.Drawing.Size(108, 31);
            this.buttonBackSpace.TabIndex = 0;
            this.buttonBackSpace.Text = "←";
            this.buttonBackSpace.UseVisualStyleBackColor = true;
            this.buttonBackSpace.Click += new System.EventHandler(this.buttonBackSpace_Click);
            // 
            // ConversorBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 457);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.display);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ConversorBase";
            this.Text = "Calculadora Programador";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button display;
        private System.Windows.Forms.RadioButton radioBin;
        private System.Windows.Forms.Label labelBin;
        private System.Windows.Forms.RadioButton radioOct;
        private System.Windows.Forms.Label labelOct;
        private System.Windows.Forms.RadioButton radioDec;
        private System.Windows.Forms.Label labelDec;
        private System.Windows.Forms.RadioButton radioHex;
        private System.Windows.Forms.Label labelHex;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button buttonC;
        private System.Windows.Forms.Button buttonBackSpace;
    }
}