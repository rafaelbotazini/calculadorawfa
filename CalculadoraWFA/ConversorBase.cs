﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Build.Conversion;

namespace CalculadoraWFA
{
    public partial class ConversorBase : Form
    {
        long dec = 0;
        bool mudouBase = false;

        public ConversorBase()
        {
            InitializeComponent();
        }

        private void ConverterBases(string s, Formatos format)
        {
            try
            {
                switch (format)
                {
                    case Formatos.Binario:
                        dec = Convert.ToInt64(s, 2);
                        break;
                    case Formatos.Decimal:
                        dec = long.Parse(s);
                        break;
                    case Formatos.Octal:
                        dec = Convert.ToInt64(s, 8);
                        break;
                    case Formatos.Hexadecimal:
                        dec = Convert.ToInt64(s, 16);
                        break;
                }
            }
            catch (Exception e)
            {
                dec = -1;
            }
        }

        private void EnterText(object sender, EventArgs e)
        {
            var btn = (Button)sender;
            if (display.Text == "0" || mudouBase)
                display.Text = "";

            display.Text += btn.Text;

            if (radioBin.Checked)
            {
                labelBin.Text = display.Text;
            }
            else if (radioOct.Checked)
            {
                labelOct.Text = display.Text;
            }
            else if (radioDec.Checked)
            {
                labelDec.Text = display.Text;
            }
            else if (radioHex.Checked)
            {
                labelHex.Text = display.Text;
            }

            mudouBase = false;
        }

        private void Calcular(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(display.Text))
            {
                if (radioBin.Checked)
                {
                    ConverterBases(display.Text, Formatos.Binario);
                }
                else if (radioOct.Checked)
                {
                    ConverterBases(display.Text, Formatos.Octal);
                }
                else if (radioDec.Checked)
                {
                    ConverterBases(display.Text, Formatos.Decimal);
                }
                else if (radioHex.Checked)
                {
                    ConverterBases(display.Text, Formatos.Hexadecimal);
                }

                labelBin.Text = Convert.ToString(dec, 2);
                labelOct.Text = Convert.ToString(dec, 8);
                labelDec.Text = Convert.ToString(dec);
                labelHex.Text = Convert.ToString(dec, 16);
            }
        }

        private void SelectBase(object sender, EventArgs e)
        {
            mudouBase = true;

            if (radioBin.Checked)
            {
                display.Text = labelBin.Text;
            }
            else if (radioOct.Checked)
            {
                display.Text = labelOct.Text;
            }
            else if (radioDec.Checked)
            {
                display.Text = labelDec.Text;
            }
            else if (radioHex.Checked)
            {
                display.Text = labelHex.Text;
            }

            bool minOct = radioOct.Checked || radioDec.Checked || radioHex.Checked;
            bool minDec = radioDec.Checked || radioHex.Checked;
            bool minHex = radioHex.Checked;

            button2.Enabled = minOct;
            button2.Enabled = minOct;
            button3.Enabled = minOct;
            button4.Enabled = minOct;
            button5.Enabled = minOct;
            button6.Enabled = minOct;
            button7.Enabled = minOct;
            button8.Enabled = minDec;
            button9.Enabled = minDec;
            button10.Enabled = minHex;
            button11.Enabled = minHex;
            button12.Enabled = minHex;
            button13.Enabled = minHex;
            button14.Enabled = minHex;
            button15.Enabled = minHex;
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            display.Text = "0";
        }

        private void buttonBackSpace_Click(object sender, EventArgs e)
        {
            string s = display.Text.Remove(display.Text.Length - 1);

            display.Text = s != "" ? s : "0";
        }
    }

    public enum Formatos
    {
        Binario = 0,
        Decimal = 1,
        Octal = 2,
        Hexadecimal = 3
    }
}
