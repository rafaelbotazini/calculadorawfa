﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculadoraWFA
{
    public class Inteiro
    {
        private int _value;
        public int Value { get { return _value; } }

        public Inteiro(int i)
        {
            _value = i;
        }

        public Inteiro(Inteiro i) : this(i.Value)
        {
        }

        public override string ToString()
        {
            return _value.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        #region Operadores
        public static Inteiro operator +(Inteiro a, Inteiro b)
        {
            return new Inteiro(a.Value + b.Value);
        }

        public static Inteiro operator -(Inteiro a, Inteiro b)
        {                              
            return new Inteiro(a.Value - b.Value);
        }

        public static Inteiro operator *(Inteiro a, Inteiro b)
        {
            return new Inteiro(a.Value * b.Value);
        }

        public static Inteiro operator /(Inteiro a, Inteiro b)
        {
            if (b.Value > 0)
                return new Inteiro(a.Value / b.Value);
            else
                throw new DivideByZeroException("Impossível dividir por 0");
        }

        public static bool operator <(Inteiro a, Inteiro b)
        {
            return a.Value < b.Value;
        }

        public static bool operator >(Inteiro a, Inteiro b)
        {
            return a.Value > b.Value;
        }
        
        public static bool operator ==(Inteiro a, Inteiro b)
        {
            return a.Value == b.Value;
        }
        
        public static bool operator !=(Inteiro a, Inteiro b)
        {
            return a.Value != b.Value;
        }
        #endregion
    }
}
